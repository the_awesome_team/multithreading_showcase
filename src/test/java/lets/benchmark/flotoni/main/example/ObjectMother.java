package lets.benchmark.flotoni.main.example;

public class ObjectMother {


    public static StoredThing aFirstStoredThing() {
        return new StoredThing(
            1,
            1,
            1
        );
    }

    public static StoredThing aSecondStoredThing() {
        return new StoredThing(
            2,
            2,
            2
        );
    }

    public static StoredThing aThirdStoredThing() {
        return new StoredThing(
            3,
            3,
            3
        );
    }

    public static StoredThing aThingWithIdOne() {
        return new StoredThing(
            1,
            1,
            1
        );
    }

    public static StoredThing aThingWithIdTwo() {
        return new StoredThing(
            2,
            2,
            2
        );
    }

    public static StoredThing anotherThingWithIdTwo() {
        return new StoredThing(
            2,
            4,
            5
        );
    }
}
