package lets.benchmark.flotoni.main.example;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

import static lets.benchmark.flotoni.main.example.ObjectMother.*;
import static lets.benchmark.flotoni.main.example.Utilities.runXTimes;
import static lets.benchmark.flotoni.main.example.Utilities.sleep;
import static org.junit.Assert.*;

@RunWith(value = Parameterized.class)
public class RepositoryTest {

    @Parameterized.Parameters(name = "{1}")
    public static Collection getAllRepoImplementations() {
        return Arrays.asList(
            new Object[]{RepositoryWithGlobalLock.class, "Global Lock"},
            new Object[]{RepositoryWithLockOnId.class, "Lock on Id"},
            new Object[]{RepositoryWithDiscardIfDirty.class, "Discard if Dirty"}
        );
    }

    private final Class<? extends Repository> repoImplementationUnderTest;
    private final String testTag;

    public RepositoryTest(Class<? extends Repository> repoImplementationUnderTest, String testTag) {
        this.repoImplementationUnderTest = repoImplementationUnderTest;
        this.testTag = testTag;
    }

    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    //Enough for our tests, we'll need at most 2-3 threads at same time
    public static final int MAX_THREADS = 100;

    private Repository repository;
    private ExecutorService threads;


    @Before
    public void setUp() throws Exception {
        repository = repoImplementationUnderTest.newInstance();
        threads = Executors.newFixedThreadPool(MAX_THREADS);
    }

    @Test
    public void saveAndGet() throws Exception {
        StoredThing first = aFirstStoredThing();
        repository.saveAndOverride(first);
        assertEquals(first, repository.get(first.id));
    }

    @Test
    public void saveDoesIndeedOverride() throws Exception {
        // Given: One is stored
        StoredThing withIdOne = new StoredThing(ONE, 1, 1);
        StoredThing anotherWithIdOne = new StoredThing(ONE, 2, 2);
        repository.saveAndOverride(withIdOne);
        assertEquals(withIdOne, repository.get(ONE));

        // When: Overriding
        repository.saveAndOverride(anotherWithIdOne);

        // Then: Is overriden
        assertNotEquals(withIdOne, repository.get(ONE));
        assertEquals(anotherWithIdOne, repository.get(ONE));
    }

    @Test
    public void getAndUpdate() throws Exception {
        // Given: First stored
        StoredThing thingWithIdOne = new StoredThing(ONE, 1, 1);
        repository.saveAndOverride(thingWithIdOne);
        assertEquals(thingWithIdOne, repository.get(ONE));

        // When: Updating
        repository.getAndUpdate(ONE, (thing) -> {
            int a = thing.A;
            int b = thing.B;
            a++;
            b--;
            return new StoredThing(
                thing.id,
                a,
                b
            );
        });

        // Then: Thing has been updated
        StoredThing fromRepo = repository.get(ONE);
        assertEquals(2, fromRepo.A);
        assertEquals(0, fromRepo.B);
        assertEquals(ONE, fromRepo.id);
    }

    @Test(expected = IllegalStateException.class)
    public void getAndUpdate_preventModificationOfId() throws Exception {
        // Given: First stored
        StoredThing aThing = aFirstStoredThing();
        repository.saveAndOverride(aThing);
        assertEquals(aThing, repository.get(ONE));

        try {
            // When: Updating AND CHANGING THE ID
            repository.getAndUpdate(ONE, (thing) -> new StoredThing(123456789, thing.A, thing.B));

            // Then: Exception raised
        } catch (IllegalStateException e) {
            assertEquals("Cannot not change the id", e.getMessage());
            throw e;
        }
    }

    @Test
    public void singleIDScenario_bothMethodShareSameLock() throws Exception {
        // Skip this test if is `RepositoryWithDiscardIfDirty`
        if (repository instanceof RepositoryWithDiscardIfDirty) return; //todo remove (create more test classes)

        runXTimes(() -> {
            // Since we're hijacking the "Runners" order, we need to re-run setup.
            setUp();

            // Given: A thing saved
            StoredThing allFieldsAt1 = new StoredThing(ONE, 1, 1);
            StoredThing allFieldsAt2 = new StoredThing(ONE, 2, 2);
            StoredThing allFieldsAt3 = new StoredThing(ONE, 3, 3);
            repository.saveAndOverride(allFieldsAt1);

            // When: Running looooooooooooooong update in one Thread
            startThread(() -> {
                repository.getAndUpdate(ONE, (thing) -> {
                    sleep(100);
                    return allFieldsAt2; // UPDATING WITH 2
                });
            });
            // And: Trying to override in another Thread
            startThread(() -> {
                repository.saveAndOverride(allFieldsAt3); // UPDATING WITH 3
            });

            /*
             * If both methods are sharing the same lock, the execution will go as follow:
             *  - Thing will be updated with `allFieldsAt2`
             *  - `saveAndOverride` will wait for the update and then:
             *  - Thing will be overriden with `allFieldsAt3`
             */
            waitForAllThreadsToFinish();
            StoredThing fromRepo = repository.get(ONE);
            assertEquals(allFieldsAt3, fromRepo);
        });
    }

    @Test
    public void multipleIDScenario_whenLockingGlobally_CanNotOverrideWhileUpdatingAnotherThing() throws Exception {
        // Skip this test if not `RepositoryWithGlobalLock`
        if (!(repository instanceof RepositoryWithGlobalLock)) return;

        runXTimes(() -> {
            // Since we're hijacking the "Runners" order, we need to re-run setup.
            setUp();

            // Given: Using the Global Lock version & 2 things is saved
            repository.saveAndOverride(aThingWithIdOne());
            repository.saveAndOverride(aThingWithIdTwo());
            Lock updateRunning = new ReentrantLock();




            // When: Running looooooooooooooong update in one Thread
            startThread(() -> {
                updateRunning.lock();
                repository.getAndUpdate(ONE, (thing) -> {
                    sleep(100);
                    return thing; // We don't care about the modification
                });
                updateRunning.unlock();
            });

            // And: Trying to override ANOTHER ID in another Thread
            StoredThing anotherThingWithIdTwo = anotherThingWithIdTwo();
            assertNotEquals(anotherThingWithIdTwo, repository.get(TWO));
            startThread(() -> {
                repository.saveAndOverride(anotherThingWithIdTwo); // Overriding ID 2
            });




            // THEN:
            // When Update is still running, Override is not applied yet
            assertFalse("Update should still be running", updateRunning.tryLock());
            assertNotEquals(anotherThingWithIdTwo, repository.get(TWO));

            // But when both threads are finished. Override was succesful.
            waitForAllThreadsToFinish();
            assertEquals(anotherThingWithIdTwo, repository.get(TWO));
        });
    }

    @Test
    public void multipleIDScenario_whenLockingPerEventId_CanOverrideOneThingWhileUpdatingAnother() throws Exception {
        // Skip this test if not `RepositoryWithLockOnId`
        if (!(repository instanceof RepositoryWithLockOnId)) return;

        runXTimes(() -> {
            // Since we're hijacking the "Runners" order, we need to re-run setup.
            setUp();

            // Given: Using the Global Lock version & 2 things is saved
            repository.saveAndOverride(aThingWithIdOne());
            repository.saveAndOverride(aThingWithIdTwo());
            final boolean[] updateRunning = new boolean[1];




            // When: Running looooooooooooooong update in one Thread
            startThread(() -> {
                updateRunning[0] = true;
                repository.getAndUpdate(ONE, (thing) -> {
                    sleep(100);
                    return thing; // We don't care about the modification
                });
                updateRunning[0] = false;
            });

            // And: Trying to override ANOTHER ID in another Thread
            StoredThing anotherThingWithIdTwo = anotherThingWithIdTwo();
            assertNotEquals(anotherThingWithIdTwo, repository.get(TWO));
            startThread(() -> {
                repository.saveAndOverride(anotherThingWithIdTwo); // Overriding ID 2
            });




            // Then: Update is still running but override is already done !!
            assertTrue("Update should still be running", updateRunning[0]);
            assertEquals("Should already be overriten", anotherThingWithIdTwo, repository.get(TWO));

            waitForAllThreadsToFinish();
            assertEquals("Should still be the overriten version", anotherThingWithIdTwo, repository.get(TWO));
        });
    }


    public static int NUMBER_OF_THINGS = 50;
//    public static int NUMBER_OF_THINGS = 500000;
//    public static int NUMBER_OF_THINGS = 50000;

    // Load for approx 100 sec (1 override per sec, 10 update per sec)
    public static int OVERRIDE_ITERATIONS = 100;
    public static int OVERRIDE_SLEEP = 100;

    public static int UPDATE_ITERATIONS = 1000;
    public static int UPDATE_SLEEP = 1;
    // We're update much faster than we override (ScoreUpdateLoader vs TimeMechanism)

    @Test
    @Ignore
    public void benchmark() throws Exception {
        Instant before = Instant.now();


        // Init repo with all events
        IntStream
            .range(1, NUMBER_OF_THINGS)
            .boxed()
            .map(id -> new StoredThing(id, 0, 0))
            .forEach(repository::saveAndOverride);



        startThread(() -> {
                IntStream.range(1, OVERRIDE_ITERATIONS)
                    .forEach(currentIteration -> {
                        // Generate X things and store them
                        IntStream
                            .range(1, NUMBER_OF_THINGS)
                            .boxed()
                            .map(id -> new StoredThing(id, currentIteration, 0))
                            .forEach(repository::saveAndOverride);

//                        sleep(OVERRIDE_SLEEP);
                    });
            });




        startThread(() -> {
            IntStream.range(1, UPDATE_ITERATIONS)
                .forEach(currentIteration -> {

                    Repository.UpdateFunction incrementBField =
                        (thing) -> new StoredThing(thing.id, thing.A, thing.B + 1);

                    // Update all things
                    IntStream
                        .range(1, NUMBER_OF_THINGS)
                        .boxed()
                        .forEach(id -> repository.getAndUpdate(id, incrementBField));

//                    sleep(UPDATE_SLEEP);
                });
        });





        waitForAllThreadsToFinish(Duration.ofHours(1));

        Instant after = Instant.now();
        Duration duration = Duration.between(before, after);
        System.out.println(testTag + " | Benchmark lasted: " + duration.toMillis() + " ms");
    }


    @Test
    public void singleIDScenario_dirtyUpdateIsRejected() throws Exception {
        if (!(repository instanceof RepositoryWithDiscardIfDirty)) return;

        runXTimes(() -> {
            // Since we're hijacking the "Runners" order, we need to re-run setup.
            setUp();

            // GIVEN: A thing saved
            StoredThing allFieldsAt1 = new StoredThing(ONE, 1, 1);
            repository.saveAndOverride(allFieldsAt1);

            boolean[] updateRunning = new boolean[1];
            DirtyThingException[] exceptionThrown = new DirtyThingException[1];



            // WHEN:
            // Running looooooooooooooong update in one Thread
            StoredThing allFieldsAt2 = new StoredThing(ONE, 2, 2);
            startThread(() -> {
                updateRunning[0] = true;
                try {
                    repository.getAndUpdate(ONE, (thing) -> {
                        sleep(100);
                        return allFieldsAt2; // UPDATING WITH 2
                    });
                } catch (DirtyThingException e) {
                    exceptionThrown[0] = e;
                }
                updateRunning[0] = false;
            });

            // and Overriding in another Thread
            StoredThing allFieldsAt3 = new StoredThing(ONE, 3, 3);
            startThread(() -> {
                repository.saveAndOverride(allFieldsAt3); // UPDATING WITH 3
            });



            // THEN:
            // Update is still running but override is already done.
            assertTrue("Update should still be running", updateRunning[0]);
            assertEquals("Should already be overriten", allFieldsAt3, repository.get(ONE));

            // And after all finish, still overriden
            waitForAllThreadsToFinish();
            assertEquals("Should still be the overriten version", allFieldsAt3, repository.get(ONE));

            // And exception was thrown when trying to update dirty thing
            assertNotNull("Exception should be thrown", exceptionThrown[0]);
            String expectedMessage = "The thing to be updated is dirty and can not be saved. " +
                "The saved version has been overriden while the update was running";
            assertEquals(expectedMessage, exceptionThrown[0].getMessage());
        });
    }

    private void startThread(Runnable runnable) {
        Utilities.startThread(threads, runnable);
    }

    private void waitForAllThreadsToFinish() throws TimeoutException {
        waitForAllThreadsToFinish(Duration.ofSeconds(2));
    }

    private void waitForAllThreadsToFinish(Duration timeout) throws TimeoutException {
        Utilities.waitForAllThreadsToFinish(threads, timeout);
    }

}