package lets.benchmark.flotoni.main.example;

import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Utilities {

    public static void startThread(ExecutorService threads, Runnable runnable) {
        threads.execute(runnable);
        sleep(10); // Wait for the thread to start
    }

    public static void waitForAllThreadsToFinish(ExecutorService threads, Duration timeout) throws TimeoutException {
        threads.shutdown();
        try {
            boolean finishedOnTime = threads.awaitTermination(timeout.toMillis(), TimeUnit.MILLISECONDS);

            if (!finishedOnTime) throw new TimeoutException("TIMEOUT !!!! Threads didn't finish !!");

        } catch (InterruptedException e) {
            throw new IllegalStateException("Should not happen!!");
        }
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new IllegalStateException("Should not happen!!");
        }
    }


    public static void runXTimes(TestFunction testFunction) throws Exception {
        int times = 2;
        for (int i = 0; i < times; i++) {
            testFunction.runTest();
        }
    }

    @FunctionalInterface
    interface TestFunction {
        void runTest() throws Exception;
    }
}
