package lets.benchmark.flotoni;

import lets.benchmark.flotoni.main.Main;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CliRunner implements CommandLineRunner {


    @Override
    public void run(String... args) throws Exception {
        Main main = new Main();
        main.main();
    }
}
