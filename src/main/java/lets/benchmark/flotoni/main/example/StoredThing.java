package lets.benchmark.flotoni.main.example;

import java.util.Objects;

public class StoredThing {

    public final int id;

    public final int A;
    public final int B;


    public StoredThing(int id, int a, int b) {
        this.id = id;
        A = a;
        B = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoredThing that = (StoredThing) o;
        return id == that.id &&
            A == that.A &&
            B == that.B;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, A, B);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("StoredThing{");
        sb.append("id=").append(id);
        sb.append(", A=").append(A);
        sb.append(", B=").append(B);
        sb.append('}');
        return sb.toString();
    }
}
