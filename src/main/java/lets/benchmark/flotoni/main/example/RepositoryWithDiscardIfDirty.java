package lets.benchmark.flotoni.main.example;

import java.util.HashMap;
import java.util.Map;

/**
 * This version doesn't lock
 * We allow writes while updating a `StoredThing`
 *
 * Only . . . when persisting the modified version of the `StoredThing`,
 * if the version we originally had has been overriden . . . the persist (or "commit") is rejected.
 *
 * We need to "pull" the newest version and redo the calculation to persist ("commit")
 *
 */
public class RepositoryWithDiscardIfDirty implements Repository {


    private final Map<Integer, StoredThing> datastore = new HashMap();

    @Override
    public void saveAndOverride(StoredThing storedThing) {
        datastore.put(storedThing.id, storedThing);
    }

    @Override
    public void getAndUpdate(int id, UpdateFunction updateFunction) {
        StoredThing current = datastore.get(id);
        StoredThing updated = updateFunction.update(current);
        if (current.id != updated.id) throw new IllegalStateException("Cannot not change the id");
        datastore.put(id, updated);
    }

    @Override
    public StoredThing get(int id) {
        return datastore.get(id);
    }
}
