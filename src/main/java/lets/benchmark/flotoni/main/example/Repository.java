package lets.benchmark.flotoni.main.example;

public interface Repository {



    void saveAndOverride(StoredThing storedThing);

    void getAndUpdate(int id, UpdateFunction updateFunction);

    StoredThing get(int id);

    @FunctionalInterface
    public interface UpdateFunction {
        StoredThing update(StoredThing fromRepo);
    }
}
