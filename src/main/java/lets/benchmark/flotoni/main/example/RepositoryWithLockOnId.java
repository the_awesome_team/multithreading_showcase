package lets.benchmark.flotoni.main.example;

import org.springframework.util.ConcurrentReferenceHashMap;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RepositoryWithLockOnId implements Repository {


    private final Map<Integer, StoredThing> datastore = new HashMap();

    private final ConcurrentMap<Integer, Lock> locks = new ConcurrentSkipListMap<>();

    @Override
    public void saveAndOverride(StoredThing storedThing) {
        lockForId(storedThing.id);

        datastore.put(storedThing.id, storedThing);

        unlockForId(storedThing.id);
    }

    private void lockForId(int id) {
        locks.compute(id, (i, lock) -> {
            if (lock == null) {
                lock = new ReentrantLock();
            }
            lock.lock();
            return lock;
        });
    }

    private void unlockForId(int id) {
        locks.computeIfPresent(id, (i, lock) -> {
            lock.unlock();
            return lock;
        });
    }


    @Override
    public void getAndUpdate(int id, UpdateFunction updateFunction) {
        lockForId(id);

        StoredThing current = datastore.get(id);
        StoredThing updated = updateFunction.update(current);
        if (current.id != updated.id) throw new IllegalStateException("Cannot not change the id");
        datastore.put(id, updated);


        unlockForId(id);
    }

    @Override
    public StoredThing get(int id) {
        return datastore.get(id);
    }


}
