package lets.benchmark.flotoni.main.example;

import java.util.HashMap;
import java.util.Map;

public class RepositoryWithGlobalLock implements Repository {
    /*
     * Do we need to explicitely synchronize `saveAndOverride` and `getAndUpdate` TOGHETHER ?
     * NO !!!! --> Done automatically.
     *             The `synchronized` keyword use a lock at the instance level.
     *             So 2 `synchronized` methods would use the same lock :)
     *             See: https://stackoverflow.com/a/15438741
     */

    private final Map<Integer, StoredThing> datastore = new HashMap();

    @Override
    public synchronized void saveAndOverride(StoredThing storedThing) {
        datastore.put(storedThing.id, storedThing);
    }

    @Override
    public synchronized void getAndUpdate(int id, UpdateFunction updateFunction) {
        StoredThing current = datastore.get(id);
        StoredThing updated = updateFunction.update(current);
        if (current.id != updated.id) throw new IllegalStateException("Cannot not change the id");
        datastore.put(id, updated);
    }

    @Override
    public StoredThing get(int id) {
        return datastore.get(id);
    }


}
