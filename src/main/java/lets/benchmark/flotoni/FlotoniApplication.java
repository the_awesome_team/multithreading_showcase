package lets.benchmark.flotoni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlotoniApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlotoniApplication.class, args);
	}
}
